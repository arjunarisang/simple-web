FROM azul/zulu-openjdk:8u232
LABEL maintainer="arjunarisang@gmail.com"
# RUN apt-get update -y && apt-get install fontconfig -y
RUN mkdir /app && ln -sf /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
EXPOSE 8080
ADD target/*.jar /app/app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Xmx64m", "-Xms32m", "-jar", "/app/app.jar"]