package co.ar.simpleweb;

import org.apache.http.client.fluent.Request;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class HelloController {

    @GetMapping(value = {"/", "hello"})
    public String hello() {
        return "Hello";
    }

    @GetMapping("/some")
    public String some() {
        return "This is some controller";
    }

    @GetMapping("/curl")
    public String curl(@RequestParam("url") String url) {
        try {
            Request.Get(url).execute();
            return "OK";
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Not OK";
    }

}
